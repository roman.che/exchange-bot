import {Context, Telegraf} from 'telegraf';
import {Update} from 'typegram';
import {connect} from 'mongoose';
import * as dotenv from 'dotenv';
import commandsHandler from './handlers/commandsHandler';

dotenv.config();

const bot: Telegraf<Context<Update>> =
    new Telegraf(process.env.TELEGRAM_BOT_TOKEN as string);

bot.use(commandsHandler);

connect(process.env.MONGODB_URL as string)
    .then(() => bot.launch());