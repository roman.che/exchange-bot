import {Context} from "telegraf";
import {User} from "../../schemas/user";

export async function isAdmin(ctx: Context, next: () => Promise<void>) {
    if (!ctx.from)
        return ctx.reply(`Sorry... somthing wrong.`);

    const user = await User.findOne({telegramId: ctx.from.id}).exec();
    if (!user)
        return ctx.reply(`Sorry... somthing wrong.`);

    if (!user.isAdmin)
        return ctx.reply(`Sorry... you don\'t have permissions`);

    return next();
}