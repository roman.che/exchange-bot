import {Context} from 'telegraf';
import {Rate} from '../../schemas/rate';

export default async function handleCurrencies(ctx: Context) {
    const rate = await Rate.findOne().exec();
    const message = Object.keys(rate?.rates as any).toString();

    return ctx.reply(message);
}