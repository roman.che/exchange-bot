import {Context} from 'telegraf';
import {IUser, User} from "../../schemas/user";

export default async function handleStart(ctx: Context) {
    if (!ctx.from)
        return ctx.reply(`Sorry... somthing wrong.`);

    const rawUser: IUser = {
        telegramId: ctx.from.id,
        firstName: ctx.from.first_name,
        lastName: ctx.from.last_name,
        username: ctx.from.username,
        languageCode: ctx.from.language_code,
        isAdmin: false
    }

    await User.findOne({telegramId: rawUser.telegramId}).exec((error, user) =>
        user ? user.updateOne(rawUser).exec() : new User(rawUser).save());

    return ctx.reply(`Hello from start command`);
}