import {Context} from 'telegraf';
import {Message} from 'telegraf/typings/core/types/typegram';
import {Rate} from '../../schemas/rate';


export default async function handleText(ctx: Context) {
    const text = (ctx.message as Message.TextMessage).text.toLowerCase();
    const regExp = new RegExp(/^(\d+)\s*(...)\s*(...)$/, 'gm');
    const parseText = regExp.exec(text);

    if (parseText === null)
        return ctx.reply(`Sorry... somthing wrong.`);

    const rate = await Rate.findOne().exec();

    if (!rate)
        return ctx.reply(`Sorry... somthing wrong.`);

    const amount = Number.parseFloat(parseText[1]);
    const fromCurrency = parseText[2].toUpperCase();
    const fromRateValue = rate.rates[fromCurrency];
    const toCurrency = parseText[3].toUpperCase();
    const toRateValue = rate.rates[toCurrency];

    if (isNaN(fromRateValue) || isNaN(toRateValue))
        return ctx.reply(`Sorry... somthing wrong.`);

    const rateValue = (amount * (toRateValue / fromRateValue));
    const fromValueFormat = new Intl.NumberFormat().format(amount)
    const toValueFormat = new Intl.NumberFormat().format(rateValue);
    const message = `${fromValueFormat} <b>${fromCurrency}</b> = ${toValueFormat} <b>${toCurrency}</b>`;

    return ctx.reply(message, {parse_mode: 'HTML'});
}