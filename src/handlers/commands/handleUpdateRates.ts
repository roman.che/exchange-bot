import {Context} from 'telegraf';
import axios from 'axios';
import {IDictionary, IRate, Rate} from '../../schemas/rate';

export default async function handleUpdateRates(ctx: Context) {
    const headers = {'apiKey': process.env.EXCHANGE_API_TOKEN};
    const {data} = await axios.get(`${process.env.EXCHANGE_URL}?${process.env.BASE_CURRENCY}`, {headers});
    const date = new Date(data.timestamp * 1000);

    const rawRate: IRate = {
        base: data.base as String,
        date,
        rates: data.rates as IDictionary<number>
    };

    await Rate.findOne().exec((error, rate) =>
        rate ? rate.updateOne(rawRate).exec() : new Rate(rawRate).save());

    return ctx.reply(`Currencies updated at: ${rawRate.date.toTimeString()}`);
}