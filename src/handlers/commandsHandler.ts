import {Composer} from 'telegraf'
import {isAdmin} from './checkers/isAdmin';
import handleCurrencies from './commands/handleCurrencies';
import handleStart from './commands/handleStart';
import handleText from './commands/handleText';
import handleUpdateRates from './commands/handleUpdateRates';
import {removeUserMessage} from "./helpers/removeUserMessage";

const composer = new Composer();
const filter = composer.filter(ctx => ctx.chat?.type === 'private');

filter.use(removeUserMessage);
filter.command('start', handleStart);
filter.command('updateRates', isAdmin, handleUpdateRates);
filter.command('currencies', handleCurrencies);
filter.on('text', handleText);

export default composer;