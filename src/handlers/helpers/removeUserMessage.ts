import { Context } from "telegraf";

export async function removeUserMessage(ctx: Context, next: () => Promise<void>) {
    await ctx.deleteMessage(ctx.message?.message_id).catch(error => console.log(error));
    return next();
}