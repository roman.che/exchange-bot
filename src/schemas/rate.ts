import {model, Schema} from "mongoose";

export interface IDictionary<TValue> {
    [id: string]: TValue;
}

export interface IRate {
    base: String;
    date: Date;
    rates: IDictionary<number>;
}

const RateSchema = new Schema<IRate>({
    base: {type: String, require: true},
    date: {type: Date, require: true},
    rates: Object
});

export const Rate = model<IRate>('rate', RateSchema);