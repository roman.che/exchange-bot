import {model, Schema} from "mongoose";

export interface IUser {
    telegramId: number;
    isAdmin: boolean;
    firstName?: string;
    lastName?: string;
    username?: string;
    languageCode?: string;
}

const UserSchema = new Schema<IUser>({
    telegramId: {type: Number, required: true},
    isAdmin: {type: Boolean, required: true},
    firstName: {type: String},
    lastName: {type: String},
    username: {type: String},
    languageCode: {type: String},
});

export const User = model<IUser>('user', UserSchema);